from flask import Flask, jsonify


app = Flask(__name__)

@app.route('/')
def test():
    print("test")
    return jsonify({'test':'flask'})

if __name__ == '__main__':
    app.run(host='0.0.0.0')